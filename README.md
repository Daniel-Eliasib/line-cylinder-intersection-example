# README

Simple example for calculating the intersection between a parametric line and an infinite cylinder defined by a point in the axis, the axis itself and it's radius.

It runs directly in the editor, it comes with handles for modifying the cylinder and the line.

The important part is the function `LineCylinderIntersection`, it needs a point in the line, the direction of the line, a point in the axis of the cylinder, the axis of the cylinder and the radius of the cylinder.
If a solution exits it returns a tuple of two floats witch describe where the intersection points are over the line equation (p + t\*v). If no solution is found it will throw an exception.

Everything else is code for rendering and adding the handles.

The relevant information for the calculations can be found [here](https://mrl.nyu.edu/~dzorin/rend05/lecture2.pdf).

## How do I get set up?

Just add this monobehavior to a GameObject and it'l render the cylinder, the line to intersect with and the handles.

## Examples:
![alternativetext](Examples/Example1.png)
![alternativetext](Examples/Example2.png)
