using System;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class LineCylinderIntersectExample : MonoBehaviour
{
    [SerializeField]
    private bool _DrawHandles = false;

    private Vector3 _CylinderBottomPoint = new Vector3(0.0f, 0.0f, 0.0f);
    private Vector3 _CylinderAxis = new Vector3(0.0f, 1.0f, 0.0f);
    private Vector3 _LinePoint = new Vector3(3.0f, 0.5f, 0.0f);
    private Vector3 _LineDirection = new Vector3(-6.0f, 0.0f, 0.0f);
    private float radii = 0.5f;
    
    private Color _SemiTransparentLine = new Color(0.2f, 0.2f, 0.2f, 0.25f);

    private void OnEnable()
    {
        SceneView.duringSceneGui += OnSceneGUI;
    }

    private void OnDisable()
    {
        SceneView.duringSceneGui -= OnSceneGUI;
    }

    private void OnSceneGUI(SceneView sv) 
    {
        if (_DrawHandles) 
        {
            EditorGUI.BeginChangeCheck();
            Quaternion oldCylRot = Quaternion.LookRotation(_CylinderAxis);

            var newLinePointPos = Handles.PositionHandle(_LinePoint, Quaternion.identity);
            var newLineDirPos = Handles.PositionHandle(_LinePoint + _LineDirection, Quaternion.identity);
            var newCylPos = Handles.PositionHandle(_CylinderBottomPoint, Quaternion.identity);
            var newCylAxisPos = Handles.PositionHandle(_CylinderBottomPoint + _CylinderAxis, Quaternion.identity);
            var newRadii = Handles.RadiusHandle(oldCylRot, _CylinderBottomPoint + _CylinderAxis*0.5f, radii);

            if (EditorGUI.EndChangeCheck())
            {
                _LinePoint = newLinePointPos;
                _LineDirection = newLineDirPos - newLinePointPos;

                radii = newRadii;

                _CylinderBottomPoint = newCylPos;
                _CylinderAxis = newCylAxisPos - newCylPos;
            }
        }
        
    }

    (float, float) LineCylinderIntersection(Vector3 _LinePoint, Vector3 _LineDirection, Vector3 _CylinderPoint, Vector3 _CylinderAxis, float radii) 
    {
        //! "va" MUST be normalized!
        // Equations pulled from https://mrl.nyu.edu/~dzorin/rend05/lecture2.pdf
        // In the pdf the (v, u) notation means dot product, and v^2 meand v dot v.
        // This is an implementation of the first part, for an infinite cylinder.
        Vector3 p = _LinePoint, v = _LineDirection, pa = _CylinderPoint, va = _CylinderAxis.normalized;

        Vector3 aux = v - Vector3.Dot(v, va) * va;
        float A = Vector3.Dot(aux, aux);

        Vector3 deltaP = p - pa;

        float B = 2 * Vector3.Dot( v - Vector3.Dot(v, va)*va, deltaP - Vector3.Dot(deltaP, va)*va);

        aux = deltaP - Vector3.Dot(deltaP, va) * va;
        float C = Vector3.Dot(aux, aux) - radii * radii;

        float disc = B * B - 4 * A * C;

        if(disc >= 0) 
        {
            float t1 = (-B - Mathf.Sqrt(disc)) / (2 * A);
            float t2 = (-B + Mathf.Sqrt(disc)) / (2 * A);

            return (t1, t2);
        }else
            throw new Exception("No points could be calculated.");
    } 

    void OnDrawGizmos()
    {
        UnityEditor.Handles.color = Color.cyan;

        //! Botoom circle
        UnityEditor.Handles.DrawWireDisc(_CylinderBottomPoint, _CylinderAxis, radii);
        //! Top circle
        UnityEditor.Handles.DrawWireDisc(_CylinderBottomPoint + _CylinderAxis, _CylinderAxis, radii);

        Gizmos.color = Color.green;
        //! Draw LinePoint
        Gizmos.DrawWireSphere(_LinePoint, 0.01f);
        Gizmos.DrawLine(_LinePoint, _LinePoint + _LineDirection);
        Gizmos.color = Color.yellow;
        //! Draw LineEndPoint
        Gizmos.DrawWireSphere(_LinePoint + _LineDirection, 0.01f);

        //! Cylinder Lines
        var downPoints = GenerateCirclePoints(_CylinderBottomPoint, _CylinderAxis, radii, 50);
        var topPoints = GenerateCirclePoints(_CylinderBottomPoint + _CylinderAxis, _CylinderAxis, radii, 50);

        Gizmos.color = _SemiTransparentLine;

        for (int i = 0; i < downPoints.Length; i++) 
        {
            Gizmos.DrawWireSphere(downPoints[i], 0.005f);
            Gizmos.DrawWireSphere(topPoints[i], 0.005f);
            Gizmos.DrawLine(downPoints[i], topPoints[i]);
        }

        //! Draw Intersectios or catches the exception when there are no intersectios.
        Gizmos.color = Color.red;

        try
        {
            (float t1, float t2) = LineCylinderIntersection(_LinePoint, _LineDirection, _CylinderBottomPoint, _CylinderAxis, radii);

            Gizmos.DrawWireSphere(_LinePoint + t1 * _LineDirection, 0.01f);
            Gizmos.DrawWireSphere(_LinePoint + t2 * _LineDirection, 0.01f);
        }
        catch 
        {
            Debug.Log("No intersections");
        }
        
    }

    Vector3[] GenerateCirclePoints(Vector3 center, Vector3 axis, float r, int res) 
    {
        Quaternion cylinderRot = Quaternion.LookRotation(axis);
        Vector3[] points = new Vector3[res];

        for(int i = 0; i < res; i++) 
        {
            float param = 2 * Mathf.PI * i / (res - 1.0f);
            points[i] = cylinderRot * new Vector3(r * Mathf.Cos(param), r * Mathf.Sin(param), 0.0f) + center;
        }

        return points;
    }
}